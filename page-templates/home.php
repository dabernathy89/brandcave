<?php
/**
 * Template Name: Home
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div id="primary" class="content-area">

    <section class="intro">
        <div class="container">
            <?php the_field('home_intro_content'); ?>
            <div class="intro-image">
                <?php echo wp_get_attachment_image( get_field('home_intro_graphic'), 'full' ); ?>
            </div>
        </div>
    </section>

    <section class="services">
        <div class="container">
            <?php the_field('home_services_description'); ?>
        </div>
    </section>

    <?php if ( have_rows('home_sections') ) : ?>
        <?php while ( have_rows('home_sections') ) : the_row(); ?>
            <?php get_template_part( 'template-parts/' . get_row_layout() ); ?>
        <?php endwhile; ?>
    <?php endif; ?>

</div><!-- #primary -->

<section class="pre-footer">
    <?php if ( get_field('display_cta_box') ) : ?>
        <?php get_template_part( 'template-parts/cta-box' ); ?>
    <?php endif; ?>
    <div class="home-footer-image">
        <?php echo wp_get_attachment_image( get_field('home_footer_image'), 'full' ); ?>
    </div>
</section>

<?php endwhile; ?>

<?php
get_footer();
