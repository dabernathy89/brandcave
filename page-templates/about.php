<?php
/**
 * Template Name: About
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Brandcave
 */

get_header(); ?>

<div id="primary" class="content-area container">
    <main id="main" class="site-main" role="main">

        <?php
        while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/content', 'page' );

            if ( have_rows('about_people') ) :
                echo '<div class="row people">';
                while ( have_rows('about_people') ) : the_row();
                    $img = wp_get_attachment_image_src( get_sub_field('image'), 'large' );
                    ?>
                    <div class="col-sm-4 person">
                        <div class="inner">
                            <div class="cover-background" style="background-image: url('<?php echo $img[0] ?>');"></div>
                            <div class="content">
                                <p class="name"><?php the_sub_field('name'); ?></p>
                                <p class="title"><?php the_sub_field('title'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
                echo '</div>';
            endif;

        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->
</div><!-- #primary -->

<section class="pre-footer">
    <?php if ( get_field('display_cta_box') ) : ?>
        <?php get_template_part( 'template-parts/cta-box' ); ?>
    <?php endif; ?>
</section>

<?php
get_footer();
