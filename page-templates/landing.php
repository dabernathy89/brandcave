<?php
/**
 * Template Name: Landing
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Brandcave
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php while ( have_posts() ) : the_post(); ?>

            <div class="landing-feature">
                <div class="entry-content">
                    <div class="inner">
                        <?php the_field('landing_left_content'); ?>
                    </div>
                </div>
            </div>
            <div class="landing-content">
                <h1 class="page-title"><?php the_field('landing_title'); ?></h1>
                <div class="divider"></div>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            </div>

        <?php endwhile; ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
