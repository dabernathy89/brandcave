<?php
/**
 * Template Name: Service
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div id="primary" class="content-area">

    <section class="intro background-type-<?php the_field('intro_background_type'); ?>">
        <div class="container">
            <?php the_field('service_intro_content'); ?>
            <div class="intro-image">
                <?php echo wp_get_attachment_image( get_field('service_intro_graphic'), 'full' ); ?>
            </div>
        </div>
    </section>

    <?php if ( have_rows('service_sections') ) : ?>
        <?php while ( have_rows('service_sections') ) : the_row(); ?>
            <?php get_template_part( 'template-parts/' . get_row_layout() ); ?>
        <?php endwhile; ?>
    <?php endif; ?>

</div><!-- #primary -->

<section class="pre-footer">
    <?php if ( get_field('display_cta_box') ) : ?>
        <?php get_template_part( 'template-parts/cta-box' ); ?>
    <?php endif; ?>
</section>

<?php endwhile; ?>

<?php
get_footer();
