var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require( 'gulp-sass' );
var autoprefixer = require('gulp-autoprefixer');

var paths = {
    "scripts": [
        "js/vendor/bootstrap/bootstrap/collapse.js",
        "js/vendor/bootstrap/bootstrap/dropdown.js",
        "js/vendor/bootstrap/bootstrap/transition.js",
        "js/vendor/fitvids.js",
        "js/main.js"
    ]
};

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(
        paths['scripts'],
        ['scripts-dev']
    );
    gulp.watch(
        ['sass/**/**/*.scss'],
        ['sass-dev']
    );
});

gulp.task('scripts-dev', function() {
    return gulp.src(paths['scripts'])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('js/build'));
});

gulp.task('scripts', function() {
    return gulp.src(paths.scripts)
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('js/build'));
});

gulp.task( 'sass-dev', function(){
    console.log('Compiling SASS (dev)');
    return gulp.src('sass/style.scss')
        .pipe(sass({
            outputStyle: 'expanded',
            precision: 8,
            sourceMap: false
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(''));
});

gulp.task( 'sass', function(){
    console.log('Compiling SASS (production)');
    return gulp.src('sass/style.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            precision: 8,
            sourceMap: false
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(''));
});

gulp.task('default', ['sass', 'scripts', 'scripts-dev']);