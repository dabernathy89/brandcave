<?php
/**
 * The header for our theme.
 *
 * This is the template that displays everything inside the body up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Brandcave
 */

get_header('opening');
?>

<header id="masthead" class="site-header" role="banner">
	<nav id="site-navigation" class="main-navigation navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".main-navigation .navbar-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<?php echo wp_get_attachment_image( get_field('primary_logo', 'options'), 'full' ); ?>
				</a>
				<p class="navbar-text"><?php the_field('nav_text', 'options'); ?></p>
			</div>

			<?php wp_nav_menu( array(
				'theme_location'    => 'primary',
				'depth'             => 2,
				'container'         => 'div',
				'container_class'   => 'collapse navbar-collapse',
				'menu_class'        => 'nav navbar-nav navbar-right',
				'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				'walker'            => new wp_bootstrap_navwalker())
			); ?>
		</div>
	</nav><!-- #site-navigation -->
</header><!-- #masthead -->

<div id="content" class="site-content">
