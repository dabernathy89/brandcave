<?php
/**
 * The blog home template file.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Brandcave
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php
    global $wp_query;
    if ( have_posts() ) :

        if ( is_home() && ! is_front_page() ) : ?>
            <header>
                <h1 class="page-title sr-only"><?php single_post_title(); ?></h1>
            </header>

        <?php
        endif;

        /* Start the Loop */
        while ( have_posts() ) : the_post();

            if ( $wp_query->current_post === 0 ) :
                get_template_part( 'template-parts/blog-header' );
            elseif ( $wp_query->current_post === 1 ) : ?>
                <div class="container">
                    <div class="row posts">
                        <div class="col-sm-6">
                            <?php get_template_part( 'template-parts/content-excerpt' ); ?>
                        </div>
            <?php
            elseif ( $wp_query->current_post === $wp_query->post_count - 1 ) : ?>
                        <div class="col-sm-6">
                            <?php get_template_part( 'template-parts/content-excerpt' ); ?>
                        </div>
                    </div><!-- row -->
                </div><!-- container -->
            <?php else : ?>
                <div class="col-sm-6">
                    <?php get_template_part( 'template-parts/content-excerpt' ); ?>
                </div>
            <?php
            endif;

        endwhile;

    else :

        get_template_part( 'template-parts/content', 'none' );

    endif; ?>

    <div class="container">
        <?php echo brandcave_paginate_links(); ?>
    </div>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
