<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Brandcave
 */

get_header(); ?>

<?php if ( has_post_thumbnail() ) : ?>
	<div
		class="featured-image-container"
		style="background-image: url('<?php the_post_thumbnail_url('brandcave-extra-large'); ?>');">
	</div>
<?php endif; ?>

<div id="primary" class="content-area container">

	<main id="main" class="site-main row" role="main">
		<div class="col-md-10 col-md-offset-1">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content' );

				if ( function_exists('gravity_form') && get_field('append_subscribe_form') ) {
					$form_id = get_field('subscribe_form_id', 'options');
					if ( $form_id ) {
						gravity_form( $form_id, false, false, false, null, true, null, $echo = true );
						gravity_form_enqueue_scripts($form_id, true);
					}
				}

			endwhile; // End of the loop.
			?>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
