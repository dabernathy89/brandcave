<?php
/**
 * Brandcave functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Brandcave
 */

if ( ! function_exists( 'brandcave_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function brandcave_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'brandcave' ),
		'footer' => esc_html__( 'Footer', 'brandcave' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
}
endif;
add_action( 'after_setup_theme', 'brandcave_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function brandcave_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'brandcave_content_width', 1200 );
}
add_action( 'after_setup_theme', 'brandcave_content_width', 0 );

function brandcave_image_sizes() {
    add_image_size( 'brandcave-extra-large', 1600 );
    add_image_size( 'brandcave-medium-square', 600, 600, true );
}
add_action( 'after_setup_theme', 'brandcave_image_sizes' );

function brandcave_submit_button_classes($button, $form){
    if ($form["button"]["type"] == "text") {
        // (class=) followed by (single or double quote) followed by (anything that is not a single or double quote)
        $pattern = '/(class=)(\'|")([^\'"]+)/';
        $replacement = '${1}${2}${3} btn btn-primary btn-submit';
        $newbutton = preg_replace($pattern, $replacement, $button);
        if ( !is_null($newbutton) ) {
            $button = $newbutton;
        }
    }
    return $button;
}
add_filter( 'gform_submit_button', 'brandcave_submit_button_classes', 10, 2 );

/**
 * Enqueue scripts and styles.
 */
function brandcave_scripts() {
	$version = "20161112";
	$url = get_site_url();

	wp_enqueue_style( 'open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' );
    wp_enqueue_style( 'simple-line-icons', 'https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css' );
	wp_enqueue_style( 'brandcave-style', get_stylesheet_uri(), array(), $version );

	if (strpos($url, '.dev') !== false || strpos($url, 'localhost') !== false) {
		wp_enqueue_script( 'brandcave-main', get_template_directory_uri() . '/js/build/scripts.js', array('jquery'), $version, true );
	} else {
		wp_enqueue_script( 'brandcave-main', get_template_directory_uri() . '/js/build/scripts.min.js', array('jquery'), $version, true );
	}
}
add_action( 'wp_enqueue_scripts', 'brandcave_scripts' );

function brandcave_acf_init() {
	acf_add_options_page( array(
		'page_title' => 'Theme Options'
	) );
}
add_action( 'acf/init', 'brandcave_acf_init' );

function brandcave_menu_classes( $atts, $item, $args ) {
    $atts['class'] = isset($atts['class']) ? $atts['class'] : '';

    foreach ($item->classes as $class_name) {
        if ( strpos($class_name, 'has-') === 0 ) {
            $atts['class'] .= substr($class_name, 4) . ' ';
        }
    }

    $atts['class'] = trim($atts['class']);

    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'brandcave_menu_classes', 10, 3 );

function brandcave_allow_svg_upload($mimes){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'brandcave_allow_svg_upload' );

function brandcave_excerpt_more( $more ) {
	if ( is_home() ) {
		return '...';
	}
    return $more;
}
add_filter( 'excerpt_more', 'brandcave_excerpt_more' );

function brandcave_excerpt_length( $length ) {
	global $wp_query;
	if ( is_home() && $wp_query->current_post === 0 ) {
		return 20;
	}

	return $length;
}
add_filter( 'excerpt_length', 'brandcave_excerpt_length', 999 );

function brandcave_add_field_icons( $content, $field, $value, $lead_id, $form_id ) {
    $field_class = str_pad( $field->cssClass, strlen( $field->cssClass ) + 1 );
    $find_icon_class = strpos( $field_class, 'has-icon');
    if ( $find_icon_class !== false ) {
        $next_space = strpos( $field_class, ' ', $find_icon_class );
        $icon_class = trim( substr( $field_class, $find_icon_class + 4 ) );
        if ( strpos( $icon_class, ' ' ) !== false ) {
            $icon_class = substr( $icon_class, 0, strpos( $icon_class, ' ' ) );
        }
        $icon_html .= '<i class="' . $icon_class . ' icons"></i>';
        $content = substr_replace( $content, $icon_html, strpos( $content, '/>' ) + 2, 0 );
    }
    return $content;
}
add_filter( 'gform_field_content', 'brandcave_add_field_icons', 15, 5 );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Bootstrap menu walker
 */
require get_template_directory() . '/inc/bootstrap-nav-walker.php';
