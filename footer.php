<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Brandcave
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-2 logo-wrap">

                <a class="" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <?php echo wp_get_attachment_image( get_field('primary_logo', 'options'), 'full' ); ?>
                </a>

            </div>
            <div class="col-sm-6 company-details">

                <address><?php the_field('address', 'options'); ?></address>

                <p class="copyright">
                    <?php the_field('copyright_text', 'options'); ?>
                    <?php if ( get_field('privacy_policy_link', 'options') ) : ?>
                        <a href="<?php the_field('privacy_policy_link', 'options'); ?>">Privacy Policy</a>
                    <?php endif; ?>
                </p>

            </div>
            <div class="col-sm-6 col-md-4 footer-menu-section">

                <?php wp_nav_menu( array(
                    'theme_location'    => 'footer',
                    'depth'             => 2,
                    'menu_class'        => 'footer-menu'
                ) ); ?>

            </div>
        </div>
    </div>
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
