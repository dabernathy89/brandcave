<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Brandcave
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('excerpt'); ?>>
    <?php if ( has_post_thumbnail() ) : ?>
        <div
            class="thumbnail-wrap cover-background"
            style="background-image: url('<?php the_post_thumbnail_url('large'); ?>');">
        </div>
    <?php endif; ?>

    <div class="entry-content <?php if ( ! has_post_thumbnail() ) : ?>no-thumbnail<?php endif; ?>">
        <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
        <a class="read-more btn btn-primary" href="<?php the_permalink(); ?>">Read More</a>
    </div><!-- .entry-content -->
</article><!-- #post-## -->
