<div class="blog-header">
    <div class="cover-background" style="background-image: url('<?php the_post_thumbnail_url('brandcave-extra-large'); ?>');"></div>
    <div class="container">
        <div class="col-sm-6 col-sm-offset-3">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h2 class="entry-title">
                    <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
                        <?php the_title(); ?>
                    </a>
                </h2>

                <div class="excerpt">
                    <?php the_excerpt(); ?>
                </div><!-- .entry-content -->

                <a class="btn btn-primary read-more" href="<?php the_permalink(); ?>">
                    Read More
                </a>
            </article><!-- #post-## -->
        </div>
    </div>
</div>