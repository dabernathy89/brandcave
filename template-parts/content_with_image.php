<section class="<?php echo get_row_layout(); ?> alignment-<?php the_sub_field('alignment'); ?> background-type-<?php the_sub_field('background_type'); ?>">
    <div class="<?php echo is_front_page() ? 'row-table-wrap' : 'container'; ?>">
        <?php ob_start(); ?>
        <?php if ( get_sub_field('video') ) : ?>
            <div class="row-media video-wrap">
                <div class="has-video">
                    <?php the_sub_field('video'); ?>
                </div>
            </div>
        <?php else : ?>
            <div class="row-media image-wrap">
                <?php echo wp_get_attachment_image( get_sub_field('image'), 'full' ); ?>
            </div>
        <?php endif; ?>
        <?php $media_content = ob_get_clean(); ?>

        <?php if ( get_sub_field('alignment') === 'right' ) : ?>
            <?php echo $media_content; ?>
        <?php endif; ?>

        <div class="text-content">
            <div class="inner">
                <?php if ( get_sub_field('title') ) : ?>
                    <p class="title"><?php the_sub_field('title'); ?></p>
                <?php endif; ?>
                <?php if ( get_sub_field('headline') ) : ?>
                    <h2 class="headline"><?php the_sub_field('headline'); ?></h2>
                <?php endif; ?>
                <?php if ( get_sub_field('description') ) : ?>
                    <div class="description"><?php the_sub_field('description'); ?></div>
                <?php endif; ?>

                <?php if ( get_sub_field('button_link') ) : ?>
                    <a class="btn btn-primary" href="<?php the_sub_field('button_link'); ?>">
                        <?php the_sub_field('button_text'); ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>

        <?php echo $media_content; ?>
    </div>
</section>