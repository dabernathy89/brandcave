<div class="cta-box-wrap container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="cta-box">
                <div class="content">
                    <h2 class="title"><?php the_field('cta_box_title'); ?></h2>
                    <?php if ( get_field('cta_button_text') ) : ?>
                        <a class="btn btn-primary" href="<?php the_field('cta_button_link'); ?>">
                            <?php the_field('cta_button_text'); ?>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="quote clearfix">
                    <?php $acf_id = get_field('cta_use_default_quote') ? 'options' : get_the_ID(); ?>
                    <div class="quote-img-wrap">
                        <?php echo wp_get_attachment_image( get_field('cta_quote_image', $acf_id), 'medium' ); ?>
                    </div>
                    <div class="quote-text">
                        <blockquote><?php the_field('cta_quote_text', $acf_id); ?></blockquote>
                        <span class="attribution"><?php the_field('cta_attribution', $acf_id); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>