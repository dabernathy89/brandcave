<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Brandcave
 */

if ( ! function_exists( 'brandcave_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function brandcave_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'on %s', 'post date', 'brandcave' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'Written by %s', 'post author', 'brandcave' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="byline"> ' . $byline . '</span> <span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'brandcave' ) );
		if ( $categories_list && brandcave_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( ' in %1$s', 'brandcave' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}
	}

}
endif;

if ( ! function_exists( 'brandcave_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function brandcave_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'brandcave' ) );
		if ( $categories_list && brandcave_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'brandcave' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'brandcave' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'brandcave' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'brandcave' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'brandcave' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function brandcave_categorized_blog() {
	return true;
}

function brandcave_paginate_links( $args = array() ) {
	$html = '<ul class="pagination">';

	$updated_args = array_merge( array(
		'prev_text' => '<span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>',
		'next_text' => '<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>',
		'type' => 'array'
	), $args );

	$links = paginate_links( $updated_args );
	foreach ($links as $link) {
		if ( strpos($link, 'current') !== false ) {
			$html .= '<li class="disabled">' . $link . '</li>';
		} else {
			$html .= '<li>' . $link . '</li>';
		}
	}

	$html .= '</ul>';

	return $html;
}

/**
 * Flush out the transients used in brandcave_categorized_blog.
 */
function brandcave_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'brandcave_categories' );
}
add_action( 'edit_category', 'brandcave_category_transient_flusher' );
add_action( 'save_post',     'brandcave_category_transient_flusher' );
