<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Brandcave
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function brandcave_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'brandcave_body_classes' );

/**
 * Retrieve the URL of the post's featured image
 *
 * @param string $size The featured image size
 * @param int|null $post_id The ID of a post to grab the featured image from
 * @return string
 */
function brandcave_get_featured_image_url( $size = 'thumbnail', $post_id = null ) {
    $thumb_url_array = wp_get_attachment_image_src(
        get_post_thumbnail_id( $post_id ),
        $size
    );

    if ( $thumb_url_array ) {
        $thumb_url = $thumb_url_array[0];
    }

    return isset( $thumb_url ) ? $thumb_url : '';
}